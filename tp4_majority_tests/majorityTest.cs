﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using tp4_majority;

namespace tp4_majority_tests
{
    [TestClass]
    public class majorityTest
    {
        [TestMethod]
        public void isAdult()
        {
            Assert.AreEqual(true, Program.isAdult(1995));
            Assert.AreEqual(false, Program.isAdult(2002));
        }
    }
}
